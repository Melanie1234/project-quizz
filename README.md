# Name
Quizz

## Add your files

cd existing_repo
git remote add origin https://gitlab.com/Melanie1234/project-quizz.git
git branch -M main
git push -uf origin main

Maquette du projet https://www.figma.com/file/ZkD6WGA572aLPLRpPuHlJ9/Quiz?node-id=0%3A1&t=4kkiFdKHduV74gyQ-0

## Description
Mon quiz a pour sujet la physique. Il a pour but d'être simple et ludique sur des sujets du quotidien.
Il se compose de 6 questions desquelles découlent 4 réponses potentielles que l'utilisateur doit sélectionner.

## Usage
Ce projet mobilise les languages HTML, CSS, TypeScript et le framework Boostrap. Il est consultable sur https://gitlab.com/Melanie1234/project-quizz.git
Une fois le projet proche de son aboutissement, je l'ai déployé via Netlify cf. https://jade-kashata-838cbe.netlify.app/

## Support
En cas d'interrogations ou de difficultés de navigation, il est possible de me contacter à l'adresse : melanie.ferer@hotmail.fr

## Roadmap
Des améliorations sont à prévoir, principalement sur l'aspect optimisation du code. En effet, mon fichier TypeScript est à retravailler pour le rendre plus lisible et optimisé.


## Project status
Ce quizz est en cours car des améliorations sont prévues. Il serait intéressant d'optimiser le fichier Typescript dans la perspective d'étoffer le questionnaire et de permettre une maintenance plus aisée.
import bravo from "../img/gagne.png";
import perdu from "../img/perdu.png";
import einsteinImg from "../img/einstein.jpg";


let questions: string[] = ["Que veut dire la formule E = MC²  ?", "Pourquoi le ciel est-il bleu ?", "Qu'est qu'un trou noir ?",
    "Connaissez-vous la formule du monoxyde de carbone ?", "Quelle est l'unité de puissance en électricité ?", "De combien de % de vide est composé un atome ?"];
let reponses = [["La formule exprime l'équivalence entre la masse et l'énergie.",
    "Elle exprime le volume de la masse au carré",
    "Elle exprime la masse de la vitesse au carré",
    "Je donne ma langue au chat"],
["Car le ciel est peint en bleu",
    "Car il s'agit de l'interaction entre la lumière du Soleil et les molécules et particules de l'atmosphère terrestre",
    "Car il s'agit du reflet de l'ocean",
    "Je donne ma langue au chat"], ["Un trou dans l'atmosphère",
    "Une étoile massive qui ne renvoie pas les rayons lumineux",
    "Un objet céleste si compact que l'intensité de son champ gravitationnel empêche toute forme de matière ou de rayonnement de s'en échapper.",
    "Je donne ma langue au chat"], ["Il s'agit de CO", "Il s'agit de CO2", "Il s'agit de NO", "Je donne ma langue au chat"], ['Le watt', "L'ohm", "Le volt", "Je donne ma langue au chat"],
["20 %", "45%", "99%", "Je donne ma langue au chat"]]

let goodAnswers = ["La formule exprime l'équivalence entre la masse et l'énergie.", "Car il s'agit de l'interaction entre la lumière du Soleil et les molécules et particules de l'atmosphère terrestre",
    "Un objet céleste si compact que l'intensité de son champ gravitationnel empêche toute forme de matière ou de rayonnement de s'en échapper.", "Il s'agit de CO", 'Le watt', "99%"]

let leSaviezVous = ["Le saviez-vous ? <br> L'équation E = mc² est une formule d'équivalence entre la masse et l'énergie, rendue célèbre par Albert Einstein avec une publication en 1905 sur la relativité restreinte.",
"Le saviez-vous ? <br> Lorsque nous observons un ciel bleu, ce que nous voyons en réalité, ce sont les ondes bleues qui sont dispersées dans notre atmosphère. La nuit, puisque la lumière blanche du Soleil n'est plus présente, le ciel n'a aucune couleur et apparaît noir.",
"Le saviez-vous ? <br> Un trou noir est créé après la mort d'une étoile très massive", "Le monoxyde de carbone est le plus simple des oxydes du carbone. La molécule est composée d'un atome de carbone et d'un atome d'oxygène ; sa formule brute s'écrit CO",
"Le saviez-vous ? <br> Le watt (W) est l'unité de mesure de la puissance électrique. Soit la quantité d'énergie pendant un temps donné, En général 1 seconde. Le terme vient du nom de l'ingénieur écossais James Watt à l'origine du développement de la machine à vapeur.", 
"Le saviez-vous ? <br> En théorie, les particules qui forment les protons et les neutrons (les quarks) sont, tout comme l'électron, des particules ponctuelles, c'est à dire des particules sans volume"]

/**
 * Variables boutons -----------------------
 */
const btnStart = document.querySelector<HTMLElement>("#btn");

const btnNext = document.querySelector<HTMLElement>(".next");
if (btnNext) {
    btnNext.style.display = "none"
}

const submit = document.querySelector<HTMLElement>(".submit")
if (submit) {
    submit.style.display = "none"
}

const voirScore = document.querySelector<HTMLButtonElement>(".voirScore")
if (voirScore) {
    voirScore.style.display = "none"
}

let main = document.querySelector<HTMLElement>("main")
if(main){
    main.style.display = "none"
}

const restartBtn = document.querySelector<HTMLButtonElement>(".restart")
if (restartBtn) {
    restartBtn.style.display = "none"
}


/**
 * Variables élements HTML -----------------------
 */
let question = document.querySelector<HTMLElement>(".question")

let reponseListe = document.querySelectorAll<HTMLInputElement>("label")
let inputRep = document.querySelectorAll<HTMLInputElement>(".inputRep");
for (const iterator of inputRep) {
    iterator.style.display = "none"
}
let card = document.querySelector<HTMLElement>("#cardSaviezVous");


let header = document.querySelector<HTMLElement>("header")
let regles = document.querySelector<HTMLElement>(".regles")
let form = document.querySelector<HTMLElement>("form")
let showScore = document.querySelector('#score')



/**
 * Variables qui stockent des données chiffrées -----------------------
 */
let n = 1
let score: number = 0;

/**
 * Quand je clique soit sur btnStart soit sur btnNext alors la fonction se met en route pour afficher la question et les réponses au fil des clics
 */
function next() {
    if (btnStart && submit ) {
        btnStart.addEventListener("click", () => {
            submit.style.display = "block"

            if(card){
                // saviezVousP.style.display = "none"

                card.style.display = "none"
            }
            
            for (const input of inputRep) {
                input.style.display = "block"
            }
            
            if (header && regles && main) {
                header.style.display = "none"
                regles.style.display = "none"
                main.style.display = "block"
            }
            if(saviezVousP && card){
                // saviezVousP.style.display = "none!important"
                card.style.display = "none!important"
            }
            if (question) {
                question.innerHTML = questions[0];
            }
            for (let i = 0; i < reponses[0].length; i++) {
                if (reponseListe) {
                    reponseListe[i].innerText = reponses[0][i]
                    inputRep[i].value = reponses[0][i]
                };
            }
        })
    }

    if (btnNext) {
        
        btnNext.addEventListener("click", () => {
            btnNext.style.display = "none"
            modalBad.style.display = "none"
            modalBadImg.style.display = "none"
            modalGood.style.display = "none"
            modalGoodImg.style.display = "none"
            if(card){
              
                card.style.display = "none"
            }
         

            if (submit && form && voirScore) {
                submit.style.display = "block"
                form.style.display = "block"
                voirScore.style.display = "block;"
            }

            if (question && n < questions.length) {
                question.innerHTML = questions[n];
            }
            for (const radio of inputRep) {
               radio.checked = false
            }
            
            for (let i = 0; i < reponses[n].length; i++) {
                if (reponseListe && inputRep) {
                    reponseListe[i].innerText = reponses[n][i]
                    inputRep[i].value = reponses[n][i]
                }
            }
            n++;

        })
    }

    if (voirScore) {
        voirScore.addEventListener("click", () => {
            /**
             * Variables nécessaires pour stocker les données lorsqu'on clique sur Voir scpre
             */
            let finalResult = document.querySelector("#finalResult")
            let divScore = document.createElement("div");
            finalResult?.append(divScore)
            let pScore = document.createElement("p");
            divScore.appendChild(pScore)
          
            let img = document.createElement("img")
            img.src = einsteinImg;
            finalResult?.append(img)
            img.style.width = "50%";
            img.style.marginLeft = "25%"
            

          
            /**
             * Elements à cacher lorsqu'on arrive à la fin du quizz
             */
          
            if (question) {
                question.style.display = "none"
            }
            voirScore.style.display = "none"

            if(card){
              
                card.style.display = "none"
            }

            if (showScore && questions && goodAnswers) {
            
                pScore.textContent = ("Félicitations, vous êtes arrivé.e à la fin de ce quiz ! Votre score est de " + (showScore.innerHTML = String(score)) + " !")
                pScore.style.textAlign = "center"
                img

                
                if (restartBtn) {
                    restartBtn.style.display = "block"
                }
            }
        })
    }
    if (restartBtn) {
        restartBtn.addEventListener("click", () => {
            location.reload()
        })
    }
}
next()



/**
 * Variables nécessaire pour la function check
 */
let answers = "";
let j =0
let div = document.querySelector<HTMLDivElement>("#good")
let modalGood = document.createElement("p");
div?.appendChild(modalGood);
let modalGoodImg = document.createElement("img");
div?.appendChild(modalGoodImg);

let divBad = document.querySelector<HTMLDivElement>("#toBad")
let modalBad = document.createElement("p");
divBad?.appendChild(modalBad);
let modalBadImg = document.createElement("img");
divBad?.appendChild(modalBadImg);
let saviezVousP = document.querySelector<HTMLDivElement>("#leSaviezVous")

const noAnswer = document.querySelector<HTMLDivElement>(".noAnswer")
if (noAnswer){
noAnswer.style.display = "none"

}


/**
 * Quand je clique sur le bouton submit, la fonction se met en route pour checker les bonnes et les mauvaises réponses
 */
function check() {

    if (submit && btnNext && voirScore) {
        submit.addEventListener("click", () => {
            if(noAnswer){
                noAnswer.style.display = "none"
            }

            for (const radio of inputRep) {
                if (radio.checked) {
                    answers = radio.value;
                    break;
                } 
            } 
            if (goodAnswers.includes(answers)) {
                submit.style.display = "none"
                btnNext.style.display = "block"
                if(card){
                    card.style.display='block'
                }

                modalGood.style.display = "inline"
                modalGood.style.margin = "50px"
                modalGoodImg.style.display = "inline"
                modalGood.innerHTML = "Gagné !";
                modalGood.style.fontSize="28px"
                modalGoodImg.src = bravo;
                modalGoodImg.alt = "2 doigts levés";
                modalGoodImg.style.height = '150px'
              
              
                if (saviezVousP && j < leSaviezVous.length) {
                    saviezVousP.innerHTML = leSaviezVous[j];
                }
                               
                score++;

                if (showScore) {
                    showScore.innerHTML = String(score)
                }
                if (form) {
                    form.style.display = "none"
                }
                if (n >= questions.length) {
                    btnNext.style.display = "none";
                    voirScore.style.display = "block";
                }
                j++
                answers = ""
               
            } else if (!answers && noAnswer) {
                noAnswer.style.display = "block"

            }
            else {
                submit.style.display = "none"
                btnNext.style.display = "block"
                if(card){
                    card.style.display='block'
                }

                modalBad.style.display = "inline";
                modalBadImg.style.display = "inline"
                modalBad.innerHTML = "Perdu !";
                modalBad.style.fontSize="28px";
                modalBadImg.src = perdu;
                modalBadImg.alt = "panneau stop";
                modalBadImg.style.height = '150px'
                if (form) {
                    form.style.display = "none"
                }
                if (saviezVousP && j < leSaviezVous.length) {
                    saviezVousP.innerHTML = leSaviezVous[j];
                }
                if (n >= questions.length) {
                    btnNext.style.display = "none";
                    voirScore.style.display = "block";
                }
                j++
                answers = ""
               
            }
            
        })
        
    }
   
}
check()